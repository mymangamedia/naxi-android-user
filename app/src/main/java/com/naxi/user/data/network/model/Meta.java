package com.naxi.user.data.network.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Meta{
  @SerializedName("metaname")
  @Expose
  private String metaname;
  @SerializedName("metavalue")
  @Expose
  private String metavalue;
  public void setMetaname(String metaname){
   this.metaname=metaname;
  }
  public String getMetaname(){
   return metaname;
  }
  public void setMetavalue(String metavalue){
   this.metavalue=metavalue;
  }
  public String getMetavalue(){
   return metavalue;
  }
}