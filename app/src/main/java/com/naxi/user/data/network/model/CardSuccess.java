package com.naxi.user.data.network.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class CardSuccess{
  @SerializedName("error")
  @Expose
  private Integer error;
  @SerializedName("message")
  @Expose
  private String message;
  public void setError(Integer error){
   this.error=error;
  }
  public Integer getError(){
   return error;
  }
  public void setMessage(String message){
   this.message=message;
  }
  public String getMessage(){
   return message;
  }
}