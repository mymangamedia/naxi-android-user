package com.naxi.user.data.network.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Card_Data {
  @SerializedName("transaction_reference")
  @Expose
  private String transaction_reference;
  @SerializedName("chargeResponseMessage")
  @Expose
  private String chargeResponseMessage;

    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("cvv")
    @Expose
    private Integer cvv;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("expirymonth")
    @Expose
    private Integer expirymonth;
    @SerializedName("IP")
    @Expose
    private String IP;
    @SerializedName("phonenumber")
    @Expose
    private Integer phonenumber;
    @SerializedName("cardno")
    @Expose
    private Long cardno;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("PBFPubKey")
    @Expose
    private String PBFPubKey;
    @SerializedName("txRef")
    @Expose
    private String txRef;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("expiryyear")
    @Expose
    private Integer expiryyear;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("redirect_url")
    @Expose
    private String redirect_url;


  public void setTransaction_reference(String transaction_reference){
   this.transaction_reference=transaction_reference;
  }
  public String getTransaction_reference(){
   return transaction_reference;
  }
  public void setChargeResponseMessage(String chargeResponseMessage){
   this.chargeResponseMessage=chargeResponseMessage;
  }
  public String getChargeResponseMessage(){
   return chargeResponseMessage;
  }
    public void setCountry(String country){
        this.country=country;
    }
    public String getCountry(){
        return country;
    }
    public void setCvv(Integer cvv){
        this.cvv=cvv;
    }
    public Integer getCvv(){
        return cvv;
    }
    public void setAmount(Integer amount){
        this.amount=amount;
    }
    public Integer getAmount(){
        return amount;
    }
    public void setFirstname(String firstname){
        this.firstname=firstname;
    }
    public String getFirstname(){
        return firstname;
    }
    public void setExpirymonth(Integer expirymonth){
        this.expirymonth=expirymonth;
    }
    public Integer getExpirymonth(){
        return expirymonth;
    }
    public void setIP(String IP){
        this.IP=IP;
    }
    public String getIP(){
        return IP;
    }
    public void setPhonenumber(Integer phonenumber){
        this.phonenumber=phonenumber;
    }
    public Integer getPhonenumber(){
        return phonenumber;
    }
    public void setCardno(Long cardno){
        this.cardno=cardno;
    }
    public Long getCardno(){
        return cardno;
    }
    public void setLastname(String lastname){
        this.lastname=lastname;
    }
    public String getLastname(){
        return lastname;
    }
    public void setPBFPubKey(String PBFPubKey){
        this.PBFPubKey=PBFPubKey;
    }
    public String getPBFPubKey(){
        return PBFPubKey;
    }
    public void setTxRef(String txRef){
        this.txRef=txRef;
    }
    public String getTxRef(){
        return txRef;
    }
    public void setMeta(Meta meta){
        this.meta=meta;
    }
    public Meta getMeta(){
        return meta;
    }
    public void setExpiryyear(Integer expiryyear){
        this.expiryyear=expiryyear;
    }
    public Integer getExpiryyear(){
        return expiryyear;
    }
    public void setCurrency(String currency){
        this.currency=currency;
    }
    public String getCurrency(){
        return currency;
    }
    public void setEmail(String email){
        this.email=email;
    }
    public String getEmail(){
        return email;
    }
    public void setRedirect_url(String redirect_url){
        this.redirect_url=redirect_url;
    }
    public String getRedirect_url(){
        return redirect_url;
    }
}