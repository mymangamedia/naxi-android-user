package com.naxi.user.ui.activity.splash;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.User;


public interface SplashIView extends MvpView{
    void onSuccess(User user);
    void onError(Throwable e);
}
