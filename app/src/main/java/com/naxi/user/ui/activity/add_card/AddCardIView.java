package com.naxi.user.ui.activity.add_card;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.CardSuccess;
import com.naxi.user.data.network.model.Card_Model;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface AddCardIView extends MvpView{
    void onSuccess(Object card);
    void onSuccess(Card_Model card);
    void onSuccess(CardSuccess card);
    void onError(Throwable e);


}
