package com.naxi.user.ui.activity.coupon;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);
    void onError(Throwable e);
}
