package com.naxi.user.ui.activity.add_card;

import com.naxi.user.base.MvpPresenter;

import java.util.HashMap;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface AddCardIPresenter<V extends AddCardIView> extends MvpPresenter<V> {
    void card(String stripeToken);
    void card_ravpay(HashMap<String, Object> parms);
    void card_ravpay_otp(HashMap<String, Object> parms);
}
