package com.naxi.user.ui.activity.passbook;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);
    void onError(Throwable e);
}
