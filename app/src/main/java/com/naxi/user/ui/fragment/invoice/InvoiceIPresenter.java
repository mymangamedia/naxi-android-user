package com.naxi.user.ui.fragment.invoice;

import com.naxi.user.base.MvpPresenter;
import java.util.HashMap;

public interface InvoiceIPresenter<V extends InvoiceIView> extends MvpPresenter<V> {
    void payment(Integer requestId, Double tips, String paymentMode);
    void updateRide(HashMap<String, Object> obj);
}
