package com.naxi.user.ui.fragment.invoice;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.Message;

public interface InvoiceIView extends MvpView{
    void onSuccess(Message message);
    void onSuccess(Object o);
    void onError(Throwable e);
}
