package com.naxi.user.ui.activity.wallet;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.AddWallet;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
}
