package com.naxi.user.ui.activity.profile;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.User;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface ProfileIView extends MvpView{
    void onSuccess(User user);
    void onError(Throwable e);
}
