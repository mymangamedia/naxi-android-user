package com.naxi.user.ui.fragment.book_ride;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView{
    void onSuccess(Object object);
    void onError(Throwable e);
    void onSuccessCoupon(PromoResponse promoResponse);
}
