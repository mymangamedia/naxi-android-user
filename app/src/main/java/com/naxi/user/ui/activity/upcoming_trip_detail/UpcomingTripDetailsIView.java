package com.naxi.user.ui.activity.upcoming_trip_detail;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);
    void onError(Throwable e);
}
