package com.naxi.user.ui.activity.add_card;

import com.naxi.user.base.BasePresenter;
import com.naxi.user.data.network.APIClient;
import com.naxi.user.data.network.model.Card_Model;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class AddCardPresenter<V extends AddCardIView> extends BasePresenter<V> implements AddCardIPresenter<V> {
    @Override
    public void card(String cardId) {

        getCompositeDisposable().add(APIClient.getAPIClient().card(cardId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object -> getMvpView().onSuccess(object),
                        throwable -> getMvpView().onError(throwable)));
    }

    @Override
    public void card_ravpay(HashMap<String, Object> parms) {

        getCompositeDisposable().add(APIClient.getAPIClient().card_ravepay(parms)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Card_Model>() {
                               @Override
                               public void accept(Card_Model object) throws Exception {
                                   AddCardPresenter.this.getMvpView().onSuccess(object);
                               }
                           },
                        throwable -> getMvpView().onError(throwable)));
    }
    @Override
    public void card_ravpay_otp(HashMap<String, Object> parms) {

        getCompositeDisposable().add(APIClient.getAPIClient().validateCharge(parms)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object -> getMvpView().onSuccess(object),
                        throwable -> getMvpView().onError(throwable)));
    }

}
