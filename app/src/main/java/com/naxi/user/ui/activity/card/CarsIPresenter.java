package com.naxi.user.ui.activity.card;

import com.naxi.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
