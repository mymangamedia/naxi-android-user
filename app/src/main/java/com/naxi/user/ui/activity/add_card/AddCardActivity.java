package com.naxi.user.ui.activity.add_card;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.naxi.user.common.InputFilterMinMax;
import com.naxi.user.data.network.model.CardSuccess;
import com.naxi.user.data.network.model.Card_Model;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.naxi.user.R;
import com.naxi.user.base.BaseActivity;
import com.naxi.user.data.SharedHelper;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCardActivity extends BaseActivity implements AddCardIView {

    // @BindView(R.id.card_form)
    // CardForm cardForm;
    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.rave_acc_holder)
    EditText rave_acc_holder;
    @BindView(R.id.rave_cardNoTv)
    EditText rave_cardNoTv;
    @BindView(R.id.rave_card_year)
    EditText rave_card_year;
    @BindView(R.id.rave_card_month)
    EditText rave_card_month;
    @BindView(R.id.rave_cvvTv)
    EditText rave_cvvTv;

    private AddCardPresenter<AddCardActivity> presenter = new AddCardPresenter<>();
    private Card_Model card;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_card;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        // Activity title will be updated after the locale has changed in Runtime
        setTitle(getString(R.string.add_card_for_payments));

        /*cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .actionLabel(getString(R.string.add_card_details))
                .setup(this);*/
        rave_card_month.setFilters(new InputFilter[]{new InputFilterMinMax(0, 12)});
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {

        if (rave_cardNoTv.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_card_number), Toast.LENGTH_SHORT).show();
            return;
        }
        if (rave_card_month.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_card_expiration_details), Toast.LENGTH_SHORT).show();
            return;
        }
        if (rave_card_year.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_card_expiration_details), Toast.LENGTH_SHORT).show();
            return;
        }
        if (rave_cvvTv.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_card_cvv), Toast.LENGTH_SHORT).show();
            return;
        }
        if (rave_acc_holder.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_account_holder), Toast.LENGTH_SHORT).show();
            return;
        }

         /* String cardNumber = cardForm.getCardNumber();
        Integer cardMonth = Integer.parseInt(cardForm.getExpirationMonth());
        Integer cardYear = Integer.parseInt(cardForm.getExpirationYear());
        String cardCvv = cardForm.getCvv();
        Log.d("CARD", "CardDetails Number: " + cardNumber + "Month: " + cardMonth + " Year: " + cardYear + " Cvv " + cardCvv);
        Card card = new Card(cardNumber, cardMonth, cardYear, cardCvv);
        addCard(card);
*/
        boolean valid = true;
        String holder = rave_acc_holder.getText().toString();
        String cvv = rave_cvvTv.getText().toString();
        String expiryMonth = rave_card_month.getText().toString();
        String expiryYear = rave_card_year.getText().toString();
        String cardNo = rave_cardNoTv.getText().toString();

        if (cvv.length() < 3) {
            valid = false;
            Toast.makeText(this, "Enter a valid cvv", Toast.LENGTH_SHORT).show();
        }
        int number = Integer.parseInt(expiryMonth);

        if (number < 1 || number > 12) {
            valid = false;
            Toast.makeText(this, "Enter a valid expiry month", Toast.LENGTH_SHORT).show();
        }

        String pattern = "^20(1[1-9]|[2-9][0-9])$";
        if (!expiryYear.matches(pattern)) {
            valid = false;
            Toast.makeText(this, "Enter a valid expiry year", Toast.LENGTH_SHORT).show();

        }

        String cardNoStripped = cardNo.replaceAll("\\s", "");
        if (cardNoStripped.length() < 12) {
            valid = false;
            Toast.makeText(this, "Enter a valid credit card number", Toast.LENGTH_SHORT).show();
        } else {
            try {
                Long parsed = Long.parseLong(cardNoStripped);
            } catch (Exception e) {
                valid = false;
                e.printStackTrace();
                Toast.makeText(this, "Enter a valid credit card number", Toast.LENGTH_SHORT).show();
            }
        }

        if (valid)
        addCard_ravpay();
    }


    private void addCard_ravpay() {
        showLoading();

        HashMap<String, Object> map = new HashMap<>();
        map.put("name", rave_acc_holder.getText().toString());
        map.put("cvv", rave_cvvTv.getText().toString());
        map.put("month", rave_card_month.getText().toString());
        map.put("year", rave_card_year.getText().toString());
        map.put("card_no", rave_cardNoTv.getText().toString().replaceAll("\\s+",""));

        Log.v("card no",rave_cardNoTv.getText().toString().replaceAll("\\s+",""));

        presenter.card_ravpay(map);
    }
    @Override
    public void onSuccess(Card_Model card) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        this.card = card;
        if (card.getRedirect().equalsIgnoreCase("otp")) {
            showOtpDialog();
        } else if(card.getRedirect().equalsIgnoreCase("pin")){
            showPinDialog();
        }
        else if(card.getRedirect().equalsIgnoreCase("address")){
            showAddressDialog();
        }
        else {
            Toast.makeText(activity(), card.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(CardSuccess card) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if(card.getMessage().equalsIgnoreCase("Success")) {
            Toast.makeText(this, getString(R.string.card_added), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void showOtpDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.ravpay_otp_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog =  builder.create();
        Button otp_btn = (Button) dialoglayout.findViewById(R.id.otpButton);
        EditText textBox = (EditText) dialoglayout.findViewById(R.id.otpEv);
        dialog.setView(dialoglayout);
        dialog.setTitle("");
        dialog.setCancelable(false);
        otp_btn.setOnClickListener(v -> {
            if(textBox.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter OTP", Toast.LENGTH_SHORT).show();
            else{
                dialog.dismiss();
                addCard_ravpay_otp();
            }
        });

        dialog.show();

    }
    private void showPinDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.ravpay_pin_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog =  builder.create();
        Button otp_btn = (Button) dialoglayout.findViewById(R.id.rave_pinButton);
        EditText textBox = (EditText) dialoglayout.findViewById(R.id.rave_pinEv);
        dialog.setView(dialoglayout);
        dialog.setTitle("");
        dialog.setCancelable(false);
        otp_btn.setOnClickListener(v -> {
            if(textBox.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Pin", Toast.LENGTH_SHORT).show();
            else{
                dialog.dismiss();
                addCard_ravpay_pin(textBox.getText().toString());
            }
        });

        dialog.show();

    }
    private void showAddressDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.ravpay_address_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog =  builder.create();
        Button otp_btn = (Button) dialoglayout.findViewById(R.id.otpButton);
        EditText address = (EditText) dialoglayout.findViewById(R.id.address);
        EditText city = (EditText) dialoglayout.findViewById(R.id.city);
        EditText state = (EditText) dialoglayout.findViewById(R.id.state);
        EditText zipcode = (EditText) dialoglayout.findViewById(R.id.zipcode);
        EditText country = (EditText) dialoglayout.findViewById(R.id.country);

        dialog.setView(dialoglayout);
        dialog.setTitle("");
        dialog.setCancelable(false);
        otp_btn.setOnClickListener(v -> {
            if(address.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Billing address", Toast.LENGTH_SHORT).show();
            else if(city.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Billing city", Toast.LENGTH_SHORT).show();
            else if(state.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Billing state", Toast.LENGTH_SHORT).show();
            else if(zipcode.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Billing zipcode", Toast.LENGTH_SHORT).show();
            else if(country.getText().toString().isEmpty())
                Toast.makeText(activity(), "Enter Billing country", Toast.LENGTH_SHORT).show();
            else{
                dialog.dismiss();
                HashMap<String, Object> map = new HashMap<>();
                map.put("name", rave_acc_holder.getText().toString());
                map.put("cvv", rave_cvvTv.getText().toString());
                map.put("month", rave_card_month.getText().toString());
                map.put("year", rave_card_year.getText().toString());
                map.put("billingcity", city.getText().toString());
                map.put("billingzip", zipcode.getText().toString());
                map.put("billingcountry", country.getText().toString());
                map.put("billingstate", state.getText().toString());
                map.put("billingaddress", address.getText().toString());
                map.put("card_no", rave_cardNoTv.getText().toString().replaceAll("\\s+",""));
                showLoading();
                presenter.card_ravpay(map);
            }
        });

        dialog.show();

    }


    private void addCard_ravpay_otp() {
        showLoading();

        HashMap<String, Object> map = new HashMap<>();
        map.put("transaction_reference", card.getData().getTransaction_reference());
        map.put("otp", rave_cvvTv.getText().toString());

        presenter.card_ravpay_otp(map);
    }

    private void addCard_ravpay_pin(String pin) {
        showLoading();

        HashMap<String, Object> map = new HashMap<>();
        map.put("name", rave_acc_holder.getText().toString());
        map.put("cvv", rave_cvvTv.getText().toString());
        map.put("month", rave_card_month.getText().toString());
        map.put("year", rave_card_year.getText().toString());
        map.put("pin", pin);
        map.put("card_no", rave_cardNoTv.getText().toString().replaceAll("\\s+",""));

        Log.v("card no",rave_cardNoTv.getText().toString().replaceAll("\\s+",""));

        presenter.card_ravpay(map);
    }



    private void addCard(Card card) {
        showLoading();
        Stripe stripe = new Stripe(this, SharedHelper.getKey(this, "stripe_publishable_key"));
        stripe.createToken(card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        Log.d("CARD:", " " + token.getId());
                        Log.d("CARD:", " " + token.getCard().getLast4());
                        String stripeToken = token.getId();
                        presenter.card(stripeToken);
                    }

                    public void onError(Exception error) {
                        try {
                            hideLoading();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    @Override
    public void onSuccess(Object card) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        Toast.makeText(this, getString(R.string.card_added), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(Throwable e) {
        handleError(e);
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }
}
