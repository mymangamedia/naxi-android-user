package com.naxi.user.ui.activity.past_trip_detail;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);
    void onError(Throwable e);
}
