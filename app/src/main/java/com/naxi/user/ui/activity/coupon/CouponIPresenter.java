package com.naxi.user.ui.activity.coupon;

import com.naxi.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
