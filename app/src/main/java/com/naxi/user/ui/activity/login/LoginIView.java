package com.naxi.user.ui.activity.login;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.ForgotResponse;
import com.naxi.user.data.network.model.Token;

public interface LoginIView extends MvpView{
    void onSuccess(Token token);
    void onSuccess(ForgotResponse object);
    void onError(Throwable e);
}
