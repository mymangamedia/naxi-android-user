package com.naxi.user.ui.activity.setting;

import com.naxi.user.base.MvpView;
import com.naxi.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
